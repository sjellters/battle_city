package client.message;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ReceiveProtocol extends Thread {

    private final Socket socket;

    public ReceiveProtocol(Socket socket) {
        super("Receive protocol");
        this.socket = socket;
    }

    public void run() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String fromServer;

            while ((fromServer = in.readLine()) != null) {
                System.out.println(fromServer);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
