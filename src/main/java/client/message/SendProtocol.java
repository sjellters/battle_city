package client.message;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SendProtocol extends Thread {
    private final Socket socket;

    public SendProtocol(Socket socket) {
        super("Send Protocol");
        this.socket = socket;
    }

    public void run() {
        try {
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));

            String fromUser;

//            while (true) {
//                if (System.in.available() != 0) {
//                    int c = System.in.read();
//                    System.out.println(c);
//                    if (c == 0x1B) {
//                        break;
//                    }
//                }
//            }

            while ((fromUser = stdIn.readLine()) != null) {
                out.println(fromUser);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
